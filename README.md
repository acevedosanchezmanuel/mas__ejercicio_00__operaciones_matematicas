# mas__ejercicio_00__operaciones_matematicas

Ejercicios de programación sobre operaciones matemáticas que pueden servir como una pequeña prueba de nivel

## Getting started

Este proyecto se compone de la rama "main", creada por defecto y que tan sólo contendrá este archivo
Readme.

La rama "master", que tan sólo incluye unas pequeñas indicaciones y que en proyectos sólo usaremos para
"entregas finales".

La rama "enunciados", que alberga los enunciados de las tareas sobre las que trabajar.

La rama "soluciones", que tendrá las soluciones propuestas a las tareas planteadas. Estas soluciones
no son las únicas posible y quizá no sean las mejores, pero son las elegidas como orientación.